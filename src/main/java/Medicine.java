import org.json.simple.JSONObject;

import java.util.ArrayList;

public class Medicine {
    private final String name;
    private final String type;
    private String dosage;
    private final String description;

    Medicine(String name, String type, String dosage, String description){
        this.name = name;
        this.type = type;
        this.dosage = dosage;
        this.description = description;
    }

    Medicine(int arrIndex){
        this.name = allMedicines.get(arrIndex).name;
        this.type = allMedicines.get(arrIndex).type;
        this.dosage = allMedicines.get(arrIndex).dosage;
        this.description = allMedicines.get(arrIndex).description;

    }

    public static ArrayList<Medicine> allMedicines = new ArrayList<Medicine>();

    public void setDosage(String newDosage) {
        this.dosage = newDosage;
    }

    public String getDosage(){return dosage;}

    public String getName(){return name;}

    public String getType(){return type;}

    public void setType(String newType) {
        this.dosage = newType;
    }

    public String getDescription(){return description;}

    public static void printMedicineName(){
        int i = 0;
        for (var m : allMedicines){
            i++;
            System.out.println(i + ": " + m.getName());
        }
    }
    public JSONObject toJsonObject(){
        JSONObject jo = new JSONObject();
        jo.put("name", name);
        jo.put("dosage", dosage);
        return jo;
    }
}
