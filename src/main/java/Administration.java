import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Administration {
   public static Patient patient;  // The currently selected patient
   public static List<Patient> patients = new ArrayList<Patient>();
   public static Healthcare roleH;
   public static boolean patientChosen = false;

   // Constructor
   Administration() {
      roleH = (new Healthcare());
      patient = (new Patient(0, "Van Puffelen", "Adriaan", LocalDate.of(2000, 10, 29), 80, 1.60));
      //list of medicines
      Medicine.allMedicines.add(new Medicine("Paracetemol", "Pijnstiller", "7 keer per dag", "Doet pijn minderen"));
      Medicine.allMedicines.add(new Medicine("Somnox", "Slaapmiddel", "3 keer per dag", "Slaap je lekker van"));
      Medicine.allMedicines.add(new Medicine("Ritalin", "Prestatieverhoger", "5 keer per dag", "Wordt je rustig van"));
      Medicine.allMedicines.add(new Medicine("THC", "Pijnstiller", "20 keer per dag", "Helpt mensen relaxen"));
      Medicine.allMedicines.add(new Medicine("Alcohol", "Pretverhoger", "1 keer per dag", "Zorgt dat mensen tot leven komen"));
   }

   // Main menu
   void menu() {
      final int STOP = 0;
      final int PRINT = 1;
      final int EDIT = 2;
      final int ADDMED = 3;
      final int ADD = 4;
      final int TREATM = 5;
      final int SELECT = 6;
      final int ADDPAT = 7;
      final int DELETPAT = 8;

         readJSON();

         while (patients.isEmpty()) {
            patient.addPatient();
         }

         var scanner = new BScanner();
         System.out.println("0:  Zorgverlener");
         System.out.println("1:  Patient");
         System.out.println("Enter role:");

         boolean access = false;
         while (!access) {
            int role = scanner.scanInt();
            if (role == 0) {
               access = true;
            } else if (role == 1) {
               roleH.setZv(false);
               access = true;
            } else {
               System.out.println("Invalid integer");
               System.out.println("Enter valid role:");
            }
         }
         if (roleH.getZv()) {
            boolean roleChosen = false;
            while (!roleChosen) {
               System.out.println("1:  Doctor");
               System.out.println("2:  Physiotherapist");
               System.out.println("Enter role:");
               var scan = new BScanner();
               int role = scan.scanInt();

               if (role == 1) {
                  roleH.doctor();
                  roleChosen = true;
               } else if (role == 2) {
                  roleH.physio();
                  roleChosen = true;
               } else {
                  System.out.println("Enter valid role: ");
               }
            }
         }

         if (roleH.getZv()) {
            patient.sortPrint();
            patient.selectPatient();
         } else {
            patient.patientId();
         }
         boolean nextCycle = true;

         while (nextCycle) {
            System.out.format("%s\n", "=".repeat(80));
            System.out.format("Current patient: ");
            patient.writeOneliner();

            ////////////////////////
            // Print menu on screen
            ////////////////////////

            System.out.println();
            System.out.format("%d:  Print  patient data\n", PRINT);
            System.out.format("%d:  Edit   patient data\n", EDIT);
            System.out.println();
            if (roleH.getZv()) {
               if (roleH.getRoleDoctor()) {
                  System.out.format("%d:  Medicine menu\n", ADDMED);
               }
               if (!roleH.getRoleDoctor()) {
                  System.out.format("%d:  (LOCKED) Medicine menu\n", ADDMED);
               }
               System.out.format("%d:  Weight measure menu\n", ADD);
               System.out.format("%d:  Treatment menu\n", TREATM);
               System.out.println();
               System.out.format("%d:  Select patient\n", SELECT);
               System.out.format("%d:  Add patient\n", ADDPAT);
               System.out.format("%d:  Delete patient\n", DELETPAT);
            }
            System.out.println();
            System.out.format("%d:  STOP (save)\n", STOP);
            ////////////////////////

            System.out.println("enter digit:");
            int choice = scanner.scanInt();
            switch (choice) {
               case STOP: // interrupt the loop
                  Patient.writeJSON();
                  nextCycle = false;
                  break;

               case PRINT:
                  patient.write();
                  break;

               case EDIT:
                  patient.editMenu();
                  break;
               case ADDMED:
                  if (roleH.getZv() && roleH.getRoleDoctor()) {
                     patient.medicineMenu();
                  }
                  break;
               case ADD:
                  if (roleH.getZv()) {
                     WeightMeasure.weightMenu();
                  }
                  break;

               case SELECT:
                  if (roleH.getZv()) {
                     patient.sortPrint();
                     patient.selectPatient();
                  }
                  break;
               case ADDPAT:
                  if (roleH.getZv()) {
                     patient.addPatient();
                  }
                  break;
               case TREATM:
                  if (roleH.getZv()) {
                     patient.treatMenu();
                  }
                  break;
               case DELETPAT:
                  if (roleH.getZv()) {
                     patient.sortPrint();
                     patient.selectPatientDel();
                  }
                  break;
               default:
                  System.out.println("Please enter a *valid* digit");
                  break;
            }
         }
      }

   public static void readJSON(){
      final File file = new File("./src/main/resources/mypatients.json");
      JSONParser parser = new JSONParser();
         try (Reader reader = new FileReader(file)) {
            if (file.length() > 0) {
               Object obj = parser.parse(reader);
               JSONArray allPatients = (JSONArray) obj;
               allPatients.forEach(pat -> parseEmployeeObject((JSONObject) pat));
            }
         } catch (FileNotFoundException e) {
            e.printStackTrace();
         } catch (IOException e) {
            e.printStackTrace();
         } catch (ParseException e) {
            e.printStackTrace();
         }
   }
   private static void parseEmployeeObject(JSONObject patient) {
      patients.add(new Patient(patient));
   }
}
