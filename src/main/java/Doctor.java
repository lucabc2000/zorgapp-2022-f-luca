import org.json.simple.JSONObject;

public class Doctor extends Healthcare {

    private String doctorTreatments;

    Doctor(String doctorTreatments){
        this.doctorTreatments = doctorTreatments;
    }

    public String getDoctorTreatments() {return doctorTreatments;}

    public JSONObject toJsonObjectD() {
        JSONObject jo = new JSONObject();
        jo.put("dTreatment", doctorTreatments);
        return jo;
    }
}
