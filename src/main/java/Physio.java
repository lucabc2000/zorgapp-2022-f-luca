import org.json.simple.JSONObject;

public class Physio extends Healthcare{

    private String physioTreatments;

    Physio(String physioTreatments){
        this.physioTreatments = physioTreatments;
    }

    public String getPhysioTreatments() {return physioTreatments;}

    public JSONObject toJsonObjectP() {
        JSONObject jo = new JSONObject();
        jo.put("pTreatment", physioTreatments);
        return jo;
    }

}
