import org.json.simple.JSONObject;
import java.time.LocalDate;

public class WeightMeasure {

    private double weight;
    private LocalDate date;

    WeightMeasure(double weight, LocalDate date) {
        this.weight = weight;
        this.date = date;
    }

    public double getWeight() {return weight;}
    public LocalDate getDate() {return date;}

    public static void weightMenu() {
        boolean nextCycle = true;
        while (nextCycle) {
            System.out.println("2:  Add new checkpoint");
            System.out.println("4:  Check weight history");
            System.out.println("6:  Remove checkpoints");
            System.out.println("Enter digit (0=return)");
            var scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice == 0){
                nextCycle = false;
            } else if (choice == 2){
                Administration.patient.addCheckpoint();
            } else if (choice == 4){
                Administration.patient.weightHistory();
            } else if (choice == 6){
                Administration.patient.removeCheckPoint();
            } else {
                System.out.println("Enter valid integer");
            }
        }
    }

    public JSONObject toJsonObject(){
        JSONObject jo = new JSONObject();
        jo.put("weight", weight);
        jo.put("date", date.toString());
        return jo;
    }
}


