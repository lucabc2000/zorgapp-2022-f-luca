import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.*;

public class Patient {
   private final int RETURN = 0;
   private final int SURNAME = 1;
   private final int FIRSTNAME = 2;
   private final int NICKNAME = 3;
   private final int DATEOFBIRTH = 4;
   private final int LENGTH = 5;
   private final int WEIGHT = 6;
   private final int NICKNAMEP = 1;

   private String surName;
   private String firstName;
   private String nickName;
   private LocalDate dateOfBirth;
   private double weight;
   private double length;
   private final int id;
   public ArrayList<Medicine> medicineList = new ArrayList<Medicine>();
   public ArrayList<WeightMeasure>patientMeasures = new ArrayList<WeightMeasure>();
   public boolean usesMedicines = false;
   public boolean hasMeasurePoints = false;
   public ArrayList<Doctor> doctorTreatmentsList = new ArrayList<Doctor>();
   public ArrayList<Physio> physioTreatmentsList = new ArrayList<Physio>();

   // Constructor
   Patient(int id, String surName, String firstName, LocalDate dateOfBirth, double weight, double length) {
      this.id = id;
      this.surName = surName;
      this.firstName = firstName;
      this.nickName = firstName; // set via separate method if needed.
      this.dateOfBirth = dateOfBirth;
      this.weight = weight;
      this.length = length;
   }

   Patient(JSONObject object) {
      JSONObject patientObject = (JSONObject) object.get("patient");
      this.id = (int) (long) patientObject.get("id");
      this.surName = (String) patientObject.get("surName");
      this.firstName = (String) patientObject.get("firstName");
      this.nickName = (String) patientObject.get("nickName");
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle( ResolverStyle.STRICT );
      LocalDate r = null;
      String sdate = (String)patientObject.get("dateOfBirth");
      r     = LocalDate.parse(sdate, formatter);
      this.dateOfBirth = r;
      this.weight = (double) patientObject.get("weight");
      this.length = (double) patientObject.get("length");
      JSONArray slideContent = (JSONArray) patientObject.get("measures");
      if (slideContent != null) {
         Iterator i = slideContent.iterator();
         while (i.hasNext()) {
            JSONObject slide = (JSONObject) i.next();
            double weight = (double) slide.get("weight");
            DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle(ResolverStyle.STRICT);
            LocalDate b = null;
            String sdateb = (String) slide.get("date");
            b = LocalDate.parse(sdateb, formatterb);
            LocalDate date = b;
            this.patientMeasures.add(new WeightMeasure(weight, date));
         }
      }
      JSONArray slideContentMedicines = (JSONArray) patientObject.get("medicines");
      if (slideContentMedicines != null){
         Iterator i = slideContentMedicines.iterator();
         while (i.hasNext()) {
            JSONObject slide = (JSONObject) i.next();
            String name = (String) slide.get("name");
            String dosage = (String) slide.get("dosage");
            String type = "";
            String description = "";
            for (var medicine : Medicine.allMedicines){
               if (name.equals(medicine.getName())) {
                  type = medicine.getType();
                  description = medicine.getDescription();
               }
            }
            this.medicineList.add(new Medicine(name, type, dosage, description));
         }
      }
      JSONArray slideContentDTreatments = (JSONArray) patientObject.get("doctorTreatments");
      if (slideContentDTreatments != null) {
          Iterator i = slideContentDTreatments.iterator();
          while (i.hasNext()) {
              JSONObject slide = (JSONObject) i.next();
              String treatment = (String) slide.get("dTreatment");
              this.doctorTreatmentsList.add(new Doctor(treatment));
          }
      }
      JSONArray slideContentPTreatments = (JSONArray) patientObject.get("physioTreatments");
      if (slideContentPTreatments != null) {
         Iterator i = slideContentPTreatments.iterator();
         while (i.hasNext()) {
            JSONObject slide = (JSONObject) i.next();
            String treatment = (String) slide.get("pTreatment");
            this.physioTreatmentsList.add(new Physio(treatment));
         }
      }
   }

   public void setFirstName(String f) {
      //auto sets firstname with uppercase
      String s1 = f.substring(0, 1).toUpperCase();
      String nameCapitalized = s1 + f.substring(1);
      firstName = nameCapitalized;
   }

   public double getLength() {
      return length;
   }

   public double getWeight() {
      return weight;
   }

   public int getId() {return id;}

   public double calcBMI() {
      return getWeight() / (getLength() * getLength());
   }

   // Handle editing of patient data
   void editMenu() {
      var scanner1 = new BScanner(); // use for numbers.
      var scanner2 = new BScanner(); // use for strings.
      var scanner3 = new BScanner(); // use for dates.
      var scanner4 = new BScanner(); // use for doubles.
      boolean nextCycle = true;
      while (nextCycle) {
         System.out.println("Patient data edit menu:");
         printEditMenuOptions();
         System.out.println("Enter digit (0=return)");

         int choice = scanner1.scanInt();
         if (Administration.roleH.getZv()) {
            switch (choice) {
               case RETURN:
                  nextCycle = false;
                  break;
               case SURNAME:
                  System.out.format("Enter new surname: (was: %s)\n", surName);
                  surName = scanner2.scanString();
                  break;

               case FIRSTNAME:
                  System.out.format("Enter new first name: (was: %s)\n", firstName);
                  setFirstName(scanner2.scanString());
                  break;

               case NICKNAME:
                  System.out.format("Enter new nickname: (was: %s)\n", nickName);
                  nickName = scanner2.scanString();
                  break;

               case DATEOFBIRTH:
                  System.out.format("Enter new date of birth (yyyy-MM-dd; was: %s)\n", dateOfBirth);
                  dateOfBirth = scanner3.scanDate();
                  break;

               case LENGTH:
                  System.out.format("Enter new length (in m; was: %.2f)", length);
                  var scanCheck = scanner4.scanDouble();
                  if (scanCheck > 0) {
                     length = scanCheck;
                  } else {
                     System.out.println("Enter valid length");
                  }
                  break;

               case WEIGHT:
                  System.out.format("Enter new weight (in kg; was: %.1f)\n", getWeight());
                  var scanCheckWeight = scanner4.scanDouble();
                  if (scanCheckWeight > 0) {
                     weight = scanCheckWeight;
                  } else {
                     System.out.println("Enter valid weight");
                  }
                  break;
               default:
                  System.out.println("Invalid entry: " + choice);
                  break;
            }
           } else if (!Administration.roleH.getZv()){
            switch(choice){
               case RETURN:
                  nextCycle = false;
                  break;

               case NICKNAMEP:
                  System.out.format("Enter new nickname: (was: %s)\n", nickName);
                  nickName = scanner2.scanString();
                  break;

               default:
                  System.out.println("Invalid entry: " + choice);
                  break;
            }
           }
      }
   }

   // Write patient data to screen.
   void printEditMenuOptions() {
      if (Administration.roleH.getZv()) {
         System.out.format("%d: %-17s %s\n", SURNAME, " Surname:", surName);
         System.out.format("%d: %-17s %s\n", FIRSTNAME, " firstName:", firstName);
         System.out.format("%d: %-17s %s\n", NICKNAME, " Nickname:", nickName);

         // YOUR CODE HERE TO CALCULATE AGE
         LocalDate currDate = LocalDate.now();
         Period years = Period.between(dateOfBirth, currDate);

         System.out.format("%d: %-17s %s (age %d)\n", DATEOFBIRTH, " Date of birth:", dateOfBirth, years.getYears());
         System.out.format("%d: %-17s %.2f\n", LENGTH, " Length:", length);
         System.out.format("%d: %-17s %.2f (bmi=%.1f)\n", WEIGHT, " Weight:", getWeight(), calcBMI());
      } else {
         System.out.format("%d: %-17s %s\n", NICKNAMEP, "Nickname: " ,nickName);
      }
   }
   // Write patient data to screen.
   void write() {
      System.out.println("===================================");
      System.out.format("%-17s %s\n", "Surname:", surName);
      System.out.format("%-17s %s\n", "firstName:", firstName);
      System.out.format("%-17s %s\n", "Nickname:", nickName);

      // YOUR CODE HER TO CALCULATE AGE
      LocalDate currDate = LocalDate.now();
      Period years = Period.between(dateOfBirth, currDate);

      System.out.format("%-17s %s (age %d)\n", "Date of birth:", dateOfBirth, years.getYears());
      System.out.format("%-17s %.2f\n", "Length:", length);
      System.out.format("%-17s %.2f (bmi=%.1f)\n", "Weight:", getWeight(), calcBMI());
      if (calcBMI() < 18.5) {
         System.out.println("BMI is too low, patient is underweight!");
      } else if (calcBMI() > 24.9) {
         System.out.println("BMI is too high, patient is overweight!");
      }
      if (Administration.patient.medicineList != null && Administration.roleH.getRoleDoctor()) {
         System.out.println();
         Administration.patient.printAllMedicines();
      }
      System.out.println("===================================");
   }

   // Write oneline info of patient to screen
   void writeOneliner() {
      System.out.format("%10s %-20s [%s]", firstName, surName, dateOfBirth.toString());
      System.out.println();
   }

   public void printNames() {
      var patientData = firstName + " " + surName;
      // check if uses medicine
      if (medicineList.size() > 0) {
         usesMedicines = true;
      } else {
         usesMedicines = false;
      }
      // check if patient has measurepoints
      if (patientMeasures.size() > 0) {
         hasMeasurePoints = true;
      } else {
         hasMeasurePoints = false;
      }
      //System.out.println(patientData + "       [" + dateOfBirth + "]" + "      [Medicines: " + yesNo(usesMedicines) + "]" + "      [Measure points: " + yesNo(hasMeasurePoints) + "]");
      System.out.format("%-25s %-30s %-30s %-10s\n", patientData, "[" + dateOfBirth + "]", "[Medicines: " + yesNo(usesMedicines) + "]" , " [Measure points: " + yesNo(hasMeasurePoints) + "]");
   }

   public void selectPatient() {
      if (Administration.patientChosen) {
         System.out.println("Enter digit: (0=return)");
      } else {
         System.out.println("Enter digit:");
      }
      boolean isPatientSelected = false;

      while (!isPatientSelected) {
         var scanner = new BScanner();
         int choice = scanner.scanInt();
         int size = Administration.patients.size();

         if (choice <= size && choice > 0) {
            Administration.patient = Administration.patients.get(choice - 1);
            isPatientSelected = true;
         } else if (choice == -1) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o1.firstName.compareTo(o2.firstName);
               }
            });
            sortPrint();
         } else if (choice == -2) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o2.firstName.compareTo(o1.firstName);
               }
            });
            sortPrint();
         } else if (choice == -3) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o2.dateOfBirth.compareTo(o1.dateOfBirth);
               }
            });
            sortPrint();
         } else if (choice == -4) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o1.dateOfBirth.compareTo(o2.dateOfBirth);
               }
            });
            sortPrint();
         } else if (choice == 0) {
            if (Administration.patientChosen) {
               isPatientSelected = true;
            } else {
               System.out.println("Invalid number");
            }
         } else {
            System.out.println("Invalid number");
         }
      }
      Administration.patientChosen = true;
   }

   public void patientId() {
      System.out.println("Enter your patientID:");
      var scanner = new BScanner();
      int choice = scanner.scanInt();
      int size = Administration.patients.size();
      if (choice <= size && choice > 0) {
         Administration.patient = Administration.patients.get(choice - 1);
      } else {
         System.out.println("Invalid userID!");
         System.exit(0);
      }
   }

   public void addPatient() {
      String aSurName = "";
      String aFirstName = "";
      LocalDate aDateOfBirth = LocalDate.of(2000,10,27);
      double aWeight = 60;
      double aLength = 1.75;
      int aId;
      if (Administration.patients.isEmpty()){
         aId = 1;
      } else {
         aId = Administration.patients.get(Administration.patients.size() - 1).getId() + 1;
      }

      var scanner2 = new BScanner(); //scan String
      var scanner3 = new BScanner(); //scan Double
      var scanner4 = new BScanner(); //scan Date

      boolean addedPatient = false;
      while (!addedPatient) {
         System.out.println("Add patient");

         //SURNAME
         System.out.println("Enter surname:");
         String aSurNameChoice = scanner2.scanString();
         if (aSurNameChoice != "") {
            aSurName = aSurNameChoice;
            //FIRSTNAME
            System.out.println("Enter firstname:");
            String aFirstNameChoice = scanner2.scanString();
            if (aFirstNameChoice != ""){
               aFirstName = aFirstNameChoice;
               //DATEOFBIRTH
               System.out.println("Enter date of birth: (yyyy-MM-dd)");
               LocalDate aDateOfBirthChoice = scanner4.scanDate();
               aDateOfBirth = aDateOfBirthChoice;
               //WEIGHT
               System.out.println("Enter weight: (in KG)");
               double aWeightChoice = scanner3.scanDouble();
               if (aWeightChoice > 0) {
                  aWeight = aWeightChoice;
                  //LENGTH
                  System.out.println("Enter length: (in meters)");
                  double aLengthChoice = scanner3.scanDouble();
                  if (aLengthChoice > 0) {
                     aLength = aLengthChoice;
                     addedPatient = true;
                  } else {
                     System.out.println("Enter valid length");
                  }
               } else {
                  System.out.println("Enter valid weight");
               }
            } else {
               System.out.println("Enter valid firstname");
            }
         } else {
            System.out.println("Enter valid surname");
         }
      }
      String s1 = aFirstName.substring(0, 1).toUpperCase();
      String nameCapitalized = s1 + aFirstName.substring(1);
      Administration.patients.add(new Patient(aId, aSurName, nameCapitalized, aDateOfBirth, aWeight, aLength));
      writeJSON();
      System.out.println("Patient has been added");
   }

   public static void writeJSON(){
      final String file = "./src/main/resources/mypatients.json";
      JSONArray patientList = new JSONArray();
      for (Patient indvPatient : Administration.patients) {
         JSONObject patientDetails = new JSONObject();
         patientDetails.put("id", indvPatient.id);
         patientDetails.put("surName", indvPatient.surName);
         patientDetails.put("firstName", indvPatient.firstName);
         patientDetails.put("nickName", indvPatient.nickName);
         patientDetails.put("dateOfBirth", indvPatient.dateOfBirth.toString());
         patientDetails.put("weight", indvPatient.weight);
         patientDetails.put("length", indvPatient.length);
         //Measurepoints
         JSONArray measures = new JSONArray();
         for (var indvWeight : indvPatient.patientMeasures){
            measures.add( indvWeight.toJsonObject());
         }
         patientDetails.put("measures", measures);

         //Medicines
         JSONArray medicines = new JSONArray();
         for (var indvMedicines : indvPatient.medicineList) {
            medicines.add(indvMedicines.toJsonObject());
         }
         patientDetails.put("medicines", medicines);

         //Doctor treatments
          JSONArray dTreatments = new JSONArray();
          for (var indvTreatments : indvPatient.doctorTreatmentsList) {
              dTreatments.add(indvTreatments.toJsonObjectD());
          }
          patientDetails.put("doctorTreatments", dTreatments);

          //Physio treatments
          JSONArray pTreatments = new JSONArray();
          for (var indvTreatments : indvPatient.physioTreatmentsList) {
              pTreatments.add(indvTreatments.toJsonObjectP());
          }
          patientDetails.put("physioTreatments", pTreatments);

         JSONObject patientObject = new JSONObject();
         patientObject.put("patient", patientDetails);
         patientList.add(patientObject);
      }
      try (FileWriter writer = new FileWriter(file)) {
         writer.write(patientList.toJSONString());
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   public void sortPrint(){
      System.out.println("================================================");
      System.out.println("To sort firstname [A-Z] enter -1");
      System.out.println("To sort firstname [Z-A] enter -2");
      System.out.println("To sort date of birth [young to old] enter -3");
      System.out.println("To sort date of birth [old to young] enter -4");
      System.out.println("================================================");
      int i = 0;
      for (var p : Administration.patients){
         i++;
         System.out.print(i + ":  ");
         p.printNames();
      }
   }
   public void scanMedicine(){
      System.out.println("Enter the medicine number you want to add:");
      var scanner = new BScanner();
      int choice = scanner.scanInt();
      if (choice <= Medicine.allMedicines.size() && choice > 0) {
         medicineList.add(new Medicine(choice - 1));
      } else {
         System.out.println("Wrong number");
      }
   }
   public void editDosage(){
      int i = 0;
      for (var a : Administration.patient.medicineList) {
         i++;
         System.out.println(i + ": " + a.getName() + " (dosage: " + a.getDosage() + ")");
      }
      var scanner = new BScanner();
      var scanner2 = new BScanner();
      int choice = scanner.scanInt();
      if (choice <= Administration.patient.medicineList.size() && choice > 0) {
         System.out.println("Enter new dosage: (Dosage was: " + Administration.patient.medicineList.get(choice - 1).getDosage() + ")");
         String dosageChoice = scanner2.scanString();
         this.medicineList.get(choice - 1).setDosage(dosageChoice);
      }
   }
   public void removeMedicine() {
      if (Administration.patient.medicineList.size() > 0) {
         int i = 0;
         for (var m : Administration.patient.medicineList) {
            i++;
            System.out.println(i + ": " + m.getName());
         }
         var scanner = new BScanner();
         int choice = scanner.scanInt();

         if (choice <= this.medicineList.size() && choice > 0) {
            this.medicineList.remove(choice - 1);
         } else {
            System.out.println("Wrong number");
         }
      } else {
         System.out.println("Patient doesn't use any medicine");
      }
   }
   public void addCheckpoint() {
      double finalWeight = 0;
      LocalDate finalDate = null;
      boolean weightMeasured = false;
      while (!weightMeasured) {
         System.out.println("Enter weight: (in KG)");
         var scanner = new BScanner();
         double w = scanner.scanDouble();
         if (w > 0) {
            finalWeight = w;
            weightMeasured = true;
         } else {
            System.out.println("Enter valid weight: (in KG)");
         }
      }
      boolean dateAdded = false;
      while (!dateAdded){
         System.out.println("Choose option:");
         System.out.println("1:  Add date of today");
         System.out.println("2:  Add date manually");
         var scannerInt = new BScanner();
         int choice = scannerInt.scanInt();
         if (choice == 1) {
            finalDate = LocalDate.now();
            dateAdded = true;
         } else if (choice == 2){
            System.out.println("Enter date: (yyyy-MM-dd)");
            var scanner = new BScanner();
            LocalDate d = scanner.scanDate();
            finalDate = d;
            dateAdded = true;
         } else {
            System.out.println("Enter valid integer");
         }
      }
      this.patientMeasures.add(new WeightMeasure(finalWeight, finalDate));
      System.out.println("Successfully added!");
   }
   public void removeCheckPoint(){
      if (patientMeasures.size() > 0){
         int i = 1;
         for (WeightMeasure measure: patientMeasures){
            System.out.println(i + ":  " + "weight: " + measure.getWeight() +  "  date: "  + measure.getDate());
            i++;
         }
         var scanner = new BScanner();
         int choice = scanner.scanInt();
         if (choice > 0 && choice <= patientMeasures.size() ) {
            patientMeasures.remove(choice - 1);
         } else {
            System.out.println("Invalid number");
         }
      } else {
         System.out.println("Patient doesn't have any checkpoints to remove");
      }
   }
   public void weightHistory(){
      //Sorts Date
      if (patientMeasures.size() > 2) {
         Comparator<WeightMeasure> compareDate =
                 (WeightMeasure o1, WeightMeasure o2) -> o1.getDate().compareTo(o2.getDate());
         Collections.sort(patientMeasures, compareDate.reversed());
      }
      System.out.println();
      System.out.println("================================================================================");
      System.out.println("{ 1 STAR(*) EQUALS 5 KG }");
      for (var a : patientMeasures) {
         System.out.print("[" + a.getDate() + "] ");
         // WEIGHT * COUNTER
         String kilo = "*";
         int count = (int) Math.round(a.getWeight());
         int calculate = count / 5;
         for (int i = 0; i < calculate; i++) {
            System.out.print(kilo);
         }
         //
         System.out.println(" (" + a.getWeight() + " KG)");
      }
      System.out.println("================================================================================");
      System.out.println();
   }
   public void printAllMedicines(){
      int i = 0;
      for (var a : medicineList){
         i++;
         System.out.println("Medicine " + i + ":");
         System.out.println("Name: " + a.getName());
         System.out.println("Type: " + a.getType());
         System.out.println("Dosage: " + a.getDosage());
         System.out.println("Description: " + a.getDescription());
         System.out.println();
      }
   }
   public String yesNo(boolean bool){
      if (bool){
         return "yes";
      } else {
         return "no";
      }
   }

   public void treatMenu() {
      boolean nextCycle = false;
      while (!nextCycle) {
         System.out.println("1:  Add treatment");
         System.out.println("2:  Delete treatment");
         System.out.println("3:  Print treatments");
         System.out.println("Enter digit: (0=return)");
            var scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice == 1) {
               addTreatment();
            } else if (choice == 2) {
               deleteTreatment();
            } else if (choice == 3) {
               printTreatment();
            }else if (choice == 0) {
              nextCycle = true;
            } else {
               System.out.println("Enter valid digit");
            }
         }
      }

   public void printTreatment(){
      System.out.println();
      System.out.println("==========================================");
      if (Administration.roleH.getRoleDoctor()){
         System.out.println("Doctor treatments:");
         int i = 0;
         for (var treat: doctorTreatmentsList) {
            i++;
            System.out.println(i + ": " + treat.getDoctorTreatments());
         }
         System.out.println("Physio treatments:");
         int x = 0;
         x++;
         for (var treat: physioTreatmentsList) {
            System.out.println(x + ": " + treat.getPhysioTreatments());
         }
      } else {
         int x = 0;
         for (var treat: physioTreatmentsList) {
            x++;
            System.out.println(x + ": " + treat.getPhysioTreatments());
         }
      }
      System.out.println("=====================================================");
      System.out.println();
   }

   public void addTreatment(){
      System.out.println("Enter the treatment:");
      var scanner = new BScanner();
      String treatment = scanner.scanString();
      if (Administration.roleH.getRoleDoctor()) {
         this.doctorTreatmentsList.add(new Doctor(treatment));
      } else {
         this.physioTreatmentsList.add(new Physio(treatment));
      }
      System.out.println("Successfully added!");
   }
   public void deleteTreatment(){
      if (Administration.roleH.getRoleDoctor()){
         if (!this.doctorTreatmentsList.isEmpty()){
            int i = 1;
            for (var treat : doctorTreatmentsList) {
               System.out.println(i + ":  " + treat.getDoctorTreatments());
               i ++;
            }
            var scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice > 0 && choice <= doctorTreatmentsList.size()) {
               doctorTreatmentsList.remove(choice - 1);
            } else {
               System.out.println("Wrong number");
            }
         } else {
            System.out.println("Patient doens't have any treatments");
         }
      } else {
         if (!this.physioTreatmentsList.isEmpty()){
            int x = 1;
            for (var treat : physioTreatmentsList) {
               System.out.println(x + ":  " + treat.getPhysioTreatments());
               x++;
            }
            var scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice > 0 && choice <= physioTreatmentsList.size()) {
               physioTreatmentsList.remove(choice - 1);
            } else {
               System.out.println("Wrong number");
            }
         } else {
            System.out.println("Patient doesn't have any treatments");
         }
      }
   }
   public void medicineMenu(){
      boolean nextCycle = false;
      while (!nextCycle) {
         System.out.println("1:  Add medicine");
         System.out.println("2:  Delete medicine");
         System.out.println("3:  Edit dosage");
         System.out.println("Enter digit (0=return)");
         var scanner = new BScanner();
         int choice = scanner.scanInt();
         if (choice == 1){
            Medicine.printMedicineName();
            scanMedicine();
         } else if (choice == 2){
            if (Administration.patient.medicineList != null) {
               Administration.patient.removeMedicine();
            } else {
               System.out.println("Patient doesn't use any medicine");
            }
         } else if (choice == 3) {
            if (Administration.patient.medicineList.size() > 0) {
               Administration.patient.editDosage();
            } else {
               System.out.println("Patient doesn't use any medicine");
            }
         } else if (choice == 0) {
           nextCycle = true;
         } else {
            System.out.println("wrong number");
         }
      }
   }
   public void selectPatientDel() {
      System.out.println("Enter digit: (0=return)");
      boolean isPatientSelected = false;

      while (!isPatientSelected) {
         var scanner = new BScanner();
         int choice = scanner.scanInt();
         int size = Administration.patients.size();

         if (choice <= size && choice > 0) {
            Administration.patients.remove(choice - 1);
            System.out.println("Patient successfully deleted");
            isPatientSelected = true;
         } else if (choice == -1) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o1.firstName.compareTo(o2.firstName);
               }
            });
            sortPrint();
         } else if (choice == -2) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o2.firstName.compareTo(o1.firstName);
               }
            });
            sortPrint();
         } else if (choice == -3) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o2.dateOfBirth.compareTo(o1.dateOfBirth);
               }
            });
            sortPrint();
         } else if (choice == -4) {
            Collections.sort(Administration.patients, new Comparator<Patient>() {
               @Override
               public int compare(Patient o1, Patient o2) {
                  return o1.dateOfBirth.compareTo(o2.dateOfBirth);
               }
            });
            sortPrint();
         } else if (choice == 0) {
            isPatientSelected = true;
         } else {
            System.out.println("Invalid number");
         }
      }
   }
}
