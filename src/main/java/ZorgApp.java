import java.io.IOException;
import java.time.LocalDateTime;

public class ZorgApp
{
   public static void main( String[] args ) throws IOException {
      System.out.println( "ZorgApp-" + "sprint4.0" );

      Administration administration = new Administration();
      administration.menu();

      System.out.println( "ZorgApp ends at " + LocalDateTime.now() );
   }
}
